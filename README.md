# sbt-dependency-updater

**sbt-dependency-updater** is an [sbt](https://www.scala-sbt.org/) plugin which keeps your dependencies up to date.

![thing](media/demo.gif)

It uses [sbt-updates](https://github.com/rtimush/sbt-updates) to get all newer available versions of your dependencies and automatically edits your configuration source code files to use those new versions.

## Installation

![](https://gitlab.com/improving-andrew/sbt-dependency-updater/-/badges/release.svg)

Use sbt-dependency-updater in your project by adding

```scala
addSbtPlugin("com.awwsmm.sbt" % "sbt-dependency-updater" % "Latest Release")
```

to your `project/plugins.sbt` file, and

```scala
  .enablePlugins(DependencyUpdaterPlugin)
```

to your `project` in your `build.sbt` file.

That's it!

## How to Use

In your terminal, run

```bash
sbt updateDependencies
```

or, in the `sbt` shell, run

```sbt
updateDependencies
```

## Configuration

**sbt-dependency-updater** can be configured with an optional `.updateDependencies.conf` file at the root of your project. Here's an example `.updateDependencies.conf` file

```hocon
options {
  check = false
  debug = false
  dryRun = false
  nonStrictSemVer = false
  silent = false
}

paths = [
  "project/*.scala"
  "project/*.sbt"
  "*.scala"
  "*.sbt"
]

update {
  never = [
    "com.foo:fighters-*"
  ]
  patchOnly = [
    "com.bar:specific-artifact",
    "org.baz:globs-*-are-fine"
  ]
  minorOnly = [
    "com.bar:another-specific-artifact",
    "org.baz:globs-*",
    "com.qux:*"
  ]
  minorAndPatchOnly = [
    "com.qux:quu-*"
  ]
}
```

### `options`

All options are `false` by default. They can be enabled by setting them to `true` in `.updateDependencies.conf`, or by passing them to `updateDependencies` as an argument, like

```sbt
updateDependencies check silent
```

**Note:** Command-line arguments do not _toggle_ options; they _enable_ them. If `nonStrictSemVer` is set to `true` in `.updateDependencies.conf`, passing `nonStrictSemVer` as a command-line argument to `updateDependencies` will have no effect, because that option is already enabled.

- `check` -- when `true`, `updateDependencies` does an `exit(1)` if there are any out-of-date dependencies; enable `check` to assert that your dependencies are up-to-date
- `debug` -- when `true`, `updateDependencies` prints lots of extra information, which may be useful for debugging
- `dryRun` -- when `true`, `updateDependencies` makes no changes to your code, and only prints to the terminal
- `nonStrictSemVer` -- when `true`, `updateDependencies` will allow updates to versions which are not strict `x.y.z` SemVer
- `silent` -- when `true`, `updateDependencies` will print no output to the terminal

Note that `debug` mode supersedes `silent` mode. 

You can omit any of these options from your `.updateDependencies.conf` file if you do not wish to set them to `true`. You can omit the entire `options` section if you do not wish to set any options to `true`.

### `paths`

The `paths` define all the places that **sbt-dependency-updater** will look for version strings (strings which look like `"x.y.z"`, where `x`, `y`, and `z` are all numeric).

You should be as specific as possible with your `paths`, because **sbt-dependency-updater** is unable to update version strings which are ambiguous (which appear in more than one place in any of the files specified by the `paths`).

The default `paths` are

```hocon
paths = [
  "project/*.scala"
  "project/*.sbt"
  "*.scala"
  "*.sbt"
]
```

**Note:** Globs (`*`) can be used here, but "double-globs" (`**`) will be treated as single globs (`*`).

You can omit the `paths` section from your `.updateDependencies.conf` if you do not wish to override the default paths. If you provide your own `paths`, they will not be appended to the default `paths`, but will _overwrite_ them.

### `update`

The `update` section of `.updateDependencies.conf` defines restrictions on updates for dependencies. Globs (`*`) can be used to match multiple dependencies.

- `never` -- dependencies in this section will _never_ be updated by **sbt-dependency-updater**
- `patchOnly` -- dependencies in this section will only receive patch-level updates
- `minorOnly` -- dependencies in this section will only receive minor-level updates (and neither major-level _nor_ patch-level updates)
- `minorAndPatchOnly` -- dependencies in this section will only receive non-major updates

For example, using the `.updateDependencies.conf` file above

- `com.foo:fighters-dave` would never be updated
- `com.bar:specific-artifact` would only receive patch-level updates

If the same dependency is matched by multiple patterns in different sections, the _most restrictive_ match will be applied. So, again looking at the example `.updateDependencies.conf` above

- `org.baz:globs-they-are-fine` would receive patch-only updates, because it matches the pattern in both the `patchOnly` and the `minorOnly` sections, but `patchOnly` is more restrictive
- `com.qux:quu-fuu` would receive minor-only updates, because it matches the pattern in both the `minorOnly` and the `minorAndPatchOnly` sections, but `minorOnly` is more restrictive. In fact, the `"com.qux:quu-*"` in the `minorAndPatchOnly` section is redundant here -- it will never apply.

Any of the `never`, `patchOnly`, `minorOnly`, and `minorAndPatchOnly` sections can be omitted from your `.updateDependencies.conf` file if you do not wish to define any dependencies at those levels.

> **Tip:** try setting `patchOnly = ["*"]` to get only patch-level updates for all dependencies.

The entire `update` section can be omitted from `.updateDependencies.conf` if you do not wish to restrict any dependency updates.

The default `update` is empty -- all dependencies will be updated to the latest version by default.

### Default Configuration

You can omit the `.updateDependencies.conf` file entirely, in which case the plugin will behave as though you had defined a file like

```hocon
options {
  check = false
  debug = false
  dryRun = false
  nonStrictSemVer = false
  silent = false
}

paths = [
  "project/*.scala"
  "project/*.sbt"
  "*.scala"
  "*.sbt"
]

update {
  never = []
  patchOnly = []
  minorOnly = []
  minorAndPatchOnly = []
}
```
