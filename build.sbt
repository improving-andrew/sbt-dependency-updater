ThisBuild / scalaVersion := "2.12.18"

lazy val root = (project in file("."))
  .settings(
    organization := "com.awwsmm.sbt",
    name := "sbt-dependency-updater",
    sbtPlugin := true,
    coverageEnabled := false, // this must be disabled when publishing; see: https://github.com/scoverage/sbt-scoverage/issues/306
    coverageFailOnMinimum := true,
    coverageMinimumStmtTotal := 100,
    coverageMinimumBranchTotal := 100,
    libraryDependencies ++= Seq(
      "org.scalactic" %% "scalactic" % Versions.scalactic,
      "org.scalatest" %% "scalatest" % Versions.scalactic % "test"
    ),
    addSbtPlugin("com.timushev.sbt" % "sbt-updates" % Versions.sbtUpdates)
  )
  .enablePlugins(SbtPlugin)