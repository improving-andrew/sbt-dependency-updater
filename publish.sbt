ThisBuild / organizationName := "improving-andrew"
ThisBuild / organizationHomepage := Some(url("https://www.awwsmm.com"))

ThisBuild / scmInfo := Some(
  ScmInfo(
    url("https://gitlab.com/improving-andrew/sbt-dependency-updater"),
    "scm:https://gitlab.com/improving-andrew/sbt-dependency-updater.git"
  )
)

ThisBuild / developers := List(
  Developer(
    id = "improving-andrew",
    name = "Andrew Watson",
    email = "aww@awwsmm.com",
    url = url("https://www.awwsmm.com")
  )
)

ThisBuild / description := "Describe your project here..."
ThisBuild / licenses := List("The Unlicense" -> new URL("https://unlicense.org/"))
ThisBuild / homepage := Some(url("https://gitlab.com/improving-andrew/sbt-dependency-updater"))

// Remove all additional repository other than Maven Central from POM
ThisBuild / pomIncludeRepository := { _ => false }

ThisBuild / versionScheme := Some("early-semver")

ThisBuild / sonatypeCredentialHost := "s01.oss.sonatype.org"
sonatypeRepository := "https://s01.oss.sonatype.org/service/local"