package com.awwsmm.sbt

import sbt.inputKey

trait DependencyUpdaterKeys {

  lazy val updateDependencies = inputKey[Unit]("updates all dependencies")

}
