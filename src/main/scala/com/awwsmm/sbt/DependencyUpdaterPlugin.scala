package com.awwsmm.sbt

import com.awwsmm.sbt.du.{Context, Helper}
import com.timushev.sbt.updates.UpdatesKeys.dependencyUpdatesData
import sbt.Def.spaceDelimited
import sbt.Keys.name
import sbt.io.IO
import sbt.{AutoPlugin, Def, File, InputTask, ThisProject, sbtUnchecked}

import java.nio.channels.FileChannel
import java.nio.file.StandardOpenOption
import java.util.concurrent.{Executors, ScheduledFuture, TimeUnit}
import scala.util.{Failure, Success, Try}

object DependencyUpdaterPlugin extends AutoPlugin {

  object autoImport extends DependencyUpdaterKeys

  import autoImport.*

  override lazy val projectSettings: Seq[Def.Setting[?]] = Seq(
    updateDependencies := {
      task.evaluated
    }
  )

  private def task: Def.Initialize[InputTask[ScheduledFuture[?]]] = Def.inputTask {

    // --------------------------------------------------------------------
    //  configuration
    // --------------------------------------------------------------------

    // pass any of "check", "debug", "dryRun", "nonStrictSemVer", "silent" as arguments
    val args = spaceDelimited().parsed

    val context = Context(args, ".updateDependencies.conf")
    import context.*

    val helper = new Helper(context)

    if (options.debug) whisper("debug mode enabled (you may see this message multiple times)")
    if (options.dryRun) whisper("dry run mode enabled -- you may see dependencies (and this message) multiple times")

    // --------------------------------------------------------------------
    //  lock dependency files so no project steps on any other one's toes (running in parallel)
    // --------------------------------------------------------------------

    // schedule retries if the dependency files are locked when this project tries to read them
    val scheduler = Executors.newScheduledThreadPool(1)

    // use the first file as a lock for all files
    val channel = FileChannel.open(paths.head, StandardOpenOption.APPEND)

    // little helper method to read file as lines
    // IO.readLines can throw a java.io.FileNotFoundException, hence the Try()
    def readFile(file: File): Option[(File, List[String])] = Try((file, IO.readLines(file))).toOption

    // try to acquire a lock on the dependency files
    val lockDepFilesAndUpdate: Runnable = () => {
      Try(channel.lock()) match {
        case Failure(_) =>
          debug(s" xxx ${(ThisProject / name).value} failed to acquire dependency file lock (will try again)")

        case Success(lock) =>
          scheduler.shutdownNow() // stop retrying once we've acquired a lock
          debug(s" >>> ${(ThisProject / name).value} acquired dependency file lock, will update dependencies...")

          // once we've got a lock, do the actual task
          try {

            val result = helper.doEverything(dependencyUpdatesData.value: @sbtUnchecked, paths.map(_.toFile).flatMap(readFile).toMap)

            // write new lines to each file only once
            if (!options.dryRun)
              result.foreach {
                case (file, newlines) =>
                  IO.write(file, newlines.mkString("\n"))
              }

          } finally {
            debug(s" <<< ${(ThisProject / name).value} releasing dependency file lock...")
            lock.close()
          }
      }
    }

    // retry every 200ms to acquire the lock, if we failed the first time
    scheduler.scheduleAtFixedRate(lockDepFilesAndUpdate, 0, 200, TimeUnit.MILLISECONDS)
  }
}
