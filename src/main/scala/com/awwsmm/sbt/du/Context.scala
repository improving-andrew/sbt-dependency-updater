package com.awwsmm.sbt.du

import com.awwsmm.sbt.du.model.{Dependency, Versions}
import com.typesafe.config.{Config, ConfigFactory}
import sbt.file
import sbt.io.IO
import sbt.nio.file.{FileTreeView, Glob}

import java.io.PrintStream
import java.nio.file.Path
import java.util.regex.Pattern
import scala.io.AnsiColor
import scala.jdk.CollectionConverters.iterableAsScalaIterableConverter
import scala.util.Try

class Context private[du] (args: Seq[String], config: Config, out: PrintStream = System.out, err: PrintStream = System.err) {
  import Context.*

  // --------------------------------------------------------------------
  //  options
  // --------------------------------------------------------------------

  private def options(option: String): Boolean = {
    args.collectFirst { case `option` =>
      true
    } orElse {
      Try(config.getBoolean(s"options.$option")).toOption
    } getOrElse false
  }

  object options {
    val check: Boolean = options("check")
    val debug: Boolean = options("debug")
    val dryRun: Boolean = options("dryRun")
    val nonStrictSemVer: Boolean = options("nonStrictSemVer")
    val silent: Boolean = options("silent")
  }

  // --------------------------------------------------------------------
  //  paths
  // --------------------------------------------------------------------

  // $COVERAGE-OFF$ -- difficult to test this without directly testing FileTreeView
  val paths: Iterable[Path] = {
    def resolvePaths(glob: String) = {
      val path = if (glob.startsWith("/")) glob else s"$cwd/$glob"
      FileTreeView.default.list(Glob(path)).map(_._1)
    }

    config.getStringList("paths").asScala.flatMap(resolvePaths)
  }
  // $COVERAGE-ON$

  // --------------------------------------------------------------------
  //  update
  // --------------------------------------------------------------------

  private def update(when: String): Dependency => Boolean = {
    val globs = Try(config.getStringList(s"update.$when").asScala).getOrElse(Seq.empty)
    createFilterFor(globs.map(toPattern))
  }

  object update {
    val patchOnly: Dependency => Boolean = update("patchOnly")

    val minorOnly: Dependency => Boolean = update("minorOnly")

    val minorAndPatchOnly: Dependency => Boolean = update("minorAndPatchOnly")

    val never: Dependency => Boolean = update("never")
  }

  // --------------------------------------------------------------------
  //  logging
  // --------------------------------------------------------------------

  def debug(x: Any): Unit = {
    val tag = s"[${AnsiColor.MAGENTA}updateDependencies debug${AnsiColor.RESET}] "
    if (options.debug) out.println(s"$tag$x".replaceAll("\n", s"\n$tag"))
  }

  def log(x: Any): Unit = {
    val tag = s"[${AnsiColor.CYAN}updateDependencies${AnsiColor.RESET}] "
    if (!options.silent) out.println(s"$tag$x".replaceAll("\n", s"\n$tag"))
  }

  def error(x: Any): Unit = err.println(x)

  def whisper(x: Any): Unit = log(s"${AnsiColor.BLACK}$x${AnsiColor.RESET}")

  // prints: will not update my-library (per .updateDependencies.conf), though major updates exist (1.2.3 -> 2.0.0)
  def willNotUpdate(organizationArtifactName: Dependency, updatesExist: String, versionAndDescendants: Versions): Unit = {
    val paths = versionAndDescendants.updatePaths
    whisper(s"will not update ${organizationArtifactName.artifact} per configuration, though $updatesExist updates exist $paths")
  }
}

object Context {

  // current working directory
  val cwd: String = new java.io.File(".").getCanonicalPath

  private val defaultConfigString = ConfigFactory.parseString(
    """paths = [
      |  "project/*.scala"
      |  "project/*.sbt"
      |  "*.scala"
      |  "*.sbt"
      |]""".stripMargin
  )

  private val strictSemVerMatcher = toPattern("^[0-9]+[.][0-9]+[.][0-9]+$")

  def isStrictSemVer(version: String): Boolean =
    strictSemVerMatcher.matcher(version).matches()

  private[du] def toPattern(string: String) = string.replaceAll("[*]", ".*").r.pattern

  def createFilterFor(patterns: Iterable[Pattern]): Dependency => Boolean =
    (organizationArtifactName: Dependency) =>
      patterns.exists(_.matcher(s"${organizationArtifactName.organization}:${organizationArtifactName.artifact}").matches())

  // $COVERAGE-OFF$
  def apply(args: Seq[String], filepath: String): Context = {
    val userConfigString = Try(IO.read(file(filepath))).map(ConfigFactory.parseString)
    val config = userConfigString.getOrElse(defaultConfigString).withFallback(defaultConfigString)
    new Context(args, config)
  }
  // $COVERAGE-ON$

}