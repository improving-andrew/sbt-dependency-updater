package com.awwsmm.sbt.du

import com.awwsmm.sbt.du.model.{Bump, DefinitionSite, Dependency, Versions}
import com.timushev.sbt.updates.versions.Version
import sbt.{File, ModuleID}

import scala.collection.SortedSet

class Helper(context: Context) {
  import Helper.*
  import context.*

  // filter all descendant versions down to user-selected candidate versions
  private def collectCandidateVersions(dependencyUpdatesData: Map[ModuleID, SortedSet[Version]]): CandidateVersions =
    dependencyUpdatesData.flatMap {
      case (moduleID, versions) =>

        // this dependency
        val (organization, artifact, version) = (moduleID.organization, moduleID.name, moduleID.revision)
        val dependency = Dependency(organization, artifact)

        // this version and any candidate (aka. descendant) versions
        val descendants = Versions(Version(version), versions)

        // ignore this dependency if we should never update it
        if (update.never(dependency)) {

          // let the user know that we're ignoring a dependency they've told us to ignore
          willNotUpdate(dependency, "one or more", descendants)

          None

        // ignore all non-SemVer candidates if the user wants to ignore them
        } else if (!options.nonStrictSemVer) {

          val filteredVersions = versions.flatMap { version =>
            if (!Context.isStrictSemVer(version.text)) {
              whisper(s"ignoring non-strict semantic version for $artifact per configuration (${descendants.current.text} -> ${version.text})")
              None
            } else {
              Some(version)
            }
          }

          Some((dependency, Versions(Version(version), filteredVersions)))

        } else {
          // use (organization, name) as Map key, rather than moduleID
          // because moduleID contains configurations, like "protobuf", and
          //   com.myorg:mylibrary:1.2.3
          // will be seen as a different dependency than
          //   com.myorg:mylibrary:1.2.3:protobuf
          // for example

          Some((dependency, Versions(Version(version), versions)))
        }
    }

  private def pairVersionsAndStrings(
    definitions: CandidateVersionStrings,
    outOfDateDependencies: CandidateVersions
  ): Map[CandidateVersions, Iterable[DefinitionSite]] =
    definitions.groupBy(_._2).flatMap {
      case (version, definitions) =>

        val dependencies = outOfDateDependencies.filter(_._2.current.text == version)
        val definitionSites = definitions.keys

        definitions.size match {
          case 0 =>
            // $COVERAGE-OFF$
            // impossible to get here after using groupBy -- it won't create an empty value for a key
            throw new Exception(s"definitions.size == 0. definitions: $definitions")
            // $COVERAGE-ON$

          case 1 =>
            // no warning here -- we've got an unambiguous definition site for this dependency
            Some(dependencies, definitionSites)

          case _ =>
            val names = dependencies.keys.toSeq.map(d => s"${d.organization}:${d.artifact}").distinct
            val br = "\n    "
            log(
              s"""Ambiguous version string "$version" found in too many places. Couldn't automatically update$br${names.mkString(br)}
                 |  Please resolve this ambiguity and try again.
                 |  Places found:$br${definitions.map(d => s"${d._1.file}:${d._1.lineIndex + 1}:").mkString(br)}""".stripMargin
            )

            Some(dependencies, definitionSites)
        }
    }

  private def calculateVersionBump(versions: Versions, dependency: Dependency): Bump = {
    val oldVersion = versions.current
    val maxPatchBump = versions.maxPatchBump
    val maxMinorBump = versions.maxMinorBump

    def willNotUpdate(updatesExist: String): Bump.Noop.type = {
      context.willNotUpdate(dependency, updatesExist, versions)
      Bump.Noop
    }

    // $COVERAGE-OFF$
    // we've already filtered out all update.never dependencies in collectCandidateVersions
    if (update.never(dependency)) {
      willNotUpdate("one or more")
    // $COVERAGE-ON$

    } else if (update.patchOnly(dependency)) {
      maxPatchBump match {
        case None => willNotUpdate("non-patch")
        case Some(newVersion) => Bump.Patch(newVersion)
      }

    } else if (update.minorOnly(dependency)) {
      maxMinorBump match {
        case None => willNotUpdate("non-minor")
        case Some(newVersion) => Bump.Minor(newVersion)
      }

    } else if (update.minorAndPatchOnly(dependency)) {
      maxMinorBump.orElse(maxPatchBump) match {
        case None => willNotUpdate("major")
        case Some(newVersion) => Bump.from(oldVersion, newVersion)
      }

    } else {
      Bump.from(oldVersion, versions.latest.get)
    }
  }

  private def doVersionBump(
    dependency: Dependency,
    versions: Versions,
    lines: Seq[String],
    index: Int,
    versionBumpCalculator: (Versions, Dependency) => Bump
  ): Seq[String] = {
    versionBumpCalculator(versions, dependency) match {
      case Bump.Noop => lines

      case bump: Bump.NonEmpty =>
        val newline = lines(index).replace(versions.current.text, bump.newVersion.text)
        val orgArt = s"${dependency.organization}:${dependency.artifact}:"

        log(s"updating $orgArt{${versions.current.text} -> ${bump.newVersion.text}} ${bump.pretty}")

        if (options.check) {
          error("failed dependency update check -- at least one dependency is out of date")
          System.exit(1)
        }

        (lines.take(index) :+ newline) ++ lines.drop(index + 1)

    }
  }

  private def editLines(
    definitions: Map[DefinitionSite, CandidateVersions],
    originalLines: Seq[String],
    versionBumpCalculator: (Versions, Dependency) => Bump = calculateVersionBump
  ): Seq[String] = {

    //  Map(
    //    line42   -> Map(a, b, c),
    //    line19   -> Map(d, e),
    //    line0    -> Map(f),
    //    line1    -> Map(f)
    //  )

//    definitions.groupBy(_._2).mapValues(_.keys)

    // Map(
    //   Map(a, b, c) -> (line42),
    //   Map(d, e)    -> (line19),
    //   Map(f)       -> (line0, line1)
    // )

    definitions.groupBy(_._2).mapValues(_.keys).foldLeft(originalLines) {

      // there is a single candidate version with a single definition site -- easy to update
      case (lines, (candidateVersions, definitionSites)) if candidateVersions.size == 1 && definitionSites.size == 1 =>
        doVersionBump(candidateVersions.head._1, candidateVersions.head._2, lines, definitionSites.head.lineIndex, versionBumpCalculator)

      // there is a single dependency, but multiple possible definition sites
      case (lines, (candidateVersions, definitionSites)) if candidateVersions.size == 1 =>

        val dependency = candidateVersions.head._1
        val version = candidateVersions.head._2.current.text

        // add a warning at each potential definition site
        definitionSites.foldLeft(lines) {
          case (lines, site) =>
            val index = site.lineIndex

            // (remove existing warning, if there already is one)
            val trimmedLine = lines(index).replaceFirst(" // TODO 'sbt updateDependencies' cannot update .*", "")

            // add a comment explaining why we couldn't update it
            val newline = trimmedLine + s""" // TODO 'sbt updateDependencies' cannot update ${dependency.organization}:${dependency.artifact} because "$version" is defined in multiple places; please update manually, and remove any unused version strings"""
            (lines.take(index) :+ newline) ++ lines.drop(index + 1)
        }

      // there are one or more definition sites, and multiple dependencies have this version
      case (lines, (candidateVersions, definitionSites)) =>

        // we should be able to update multiple dependencies if they are all updating from and to the same version
        //      e.g.
        //        val foo = "0.0.0" -> "0.0.1"
        //        val bar = "0.0.0" -> "0.0.1"
        //      in this special case, it doesn't matter if the version strings are ambiguous

        val allSameInitialVersion = candidateVersions.map(_._2.current).toSet.size == 1
        val allSameTargetVersion = candidateVersions.map(each => versionBumpCalculator(each._2, each._1)).toSet.size == 1

        definitionSites.foldLeft(lines) {
          case (lines, site) =>
            val index = site.lineIndex

            if (allSameInitialVersion && allSameTargetVersion) {
              doVersionBump(candidateVersions.head._1, candidateVersions.head._2, lines, index, versionBumpCalculator)

            } else {

              // (remove existing warning, if there already is one)
              val trimmedLine = lines(index).replaceFirst(" // TODO 'sbt updateDependencies' cannot update .*", "")

              // add a comment explaining why we couldn't update it
              val current = candidateVersions.head._2.current.text
              val owners = candidateVersions.keys.map(v => s"${v.organization}:${v.artifact}").mkString(", ")
              val newline = trimmedLine + s""" // TODO 'sbt updateDependencies' cannot update $owners because "$current" is ambiguous (it is being referenced by multiple dependencies), please update manually"""
              (lines.take(index) :+ newline) ++ lines.drop(index + 1)
            }
        }
    }
  }

  // allows us to do (nearly) end-to-end testing
  def doEverything(dependencyUpdatesData: Map[ModuleID, SortedSet[Version]], filesAndLines: Map[File, Seq[String]]): Map[File, Seq[String]] = {

    // --------------------------------------------------------------------
    //  get out-of-date dependencies
    // --------------------------------------------------------------------

    // these are all the dependencies which can possibly be updated
    // dependencyUpdatesData comes from the sbt-updates plugin
    // use @sbtUnchecked or sbt warns us about evaluating dependencyUpdatesData.value at this location
    val userFilteredVersions = collectCandidateVersions(dependencyUpdatesData)

    // --------------------------------------------------------------------
    //  locate where out-of-date dependency version strings are defined
    // --------------------------------------------------------------------

    // for each dependency, read each file and try to find where the version string is defined
    val definitions = Helper.collectCandidateVersionStrings(userFilteredVersions.map(_._2.current), filesAndLines)

    // --------------------------------------------------------------------
    //  determine which dependencies can be updated
    // --------------------------------------------------------------------

    // "x.y.z" is ambiguous if two artifacts both use that same version string, for example
    //   val fooer = "1.2.0"  // impossible to tell which version string is associated
    //   val barer = "1.2.0"  // with which artifact without more complex analysis
    val updatableDefinitions = pairVersionsAndStrings(definitions, userFilteredVersions)

    // --------------------------------------------------------------------
    //  update dependencies
    // --------------------------------------------------------------------

    Helper.groupUpdatableDefinitionsByFile(updatableDefinitions).map {
      case (file, definitions) =>

        // read each file only once
        val originalLines = filesAndLines(file)

        // loop through all of the updatableDefinitions, and edit those lines in memory
        file -> editLines(definitions, originalLines)
    }
  }

}

object Helper {

  private type CandidateVersions = Map[Dependency, Versions]
  private type CandidateVersionStrings = Map[DefinitionSite, String]

  // for each dependency, read each file and try to find where the version string is defined
  private def collectCandidateVersionStrings(
    versions: Iterable[Version],
    filesAndLines: Iterable[(File, Seq[String])]
  ): CandidateVersionStrings =

    // versions == outOfDateDependencies.map(_._2.current)
    // filesAndLines == paths.map(_.toFile).flatMap(readFile)

    versions.flatMap { version =>
      filesAndLines.flatMap {
        case (file, lines) =>
          // all we can look for is a string like "x.y.z"
          lines.zipWithIndex.collect {
            case (line, lineIndex) if line contains s""""${version.text}"""" =>
              DefinitionSite(file, lineIndex) -> version.text
          }
      }

      // toMap below to eliminate duplicates. A version definition site may be used by multiple
      // dependencies, for example:
      //   val akka = "2.7.0"
      // may be used by akka, akka-http, akka-streams, etc.
    }.toMap

  //  dependencies  definition sites
  //   vvvvvvvvvvv    vvvvvvvvvvv
  // [
  //   [ a, b, c ] -> [ x, y, z ],
  //   [ d, e ]    -> [ v, w ],
  //   [ f ]       -> [ t, u ]
  // ]
  //  => .map(_.swap) =>
  // [
  //   [ x, y, z ] -> [ a, b, c ],
  //   [ v, w ]    -> [ d, e ],
  //   [ t, u ]    -> [ f ]
  // ]
  //  => .flatMap(x => x._1.map(_ -> x._2)) =>
  // [
  //   x -> [ a, b, c ],
  //   y -> [ a, b, c ],
  //   z -> [ a, b, c ],
  //   v -> [ d, e ],
  //   w -> [ d, e ],
  //   t -> [ f ],
  //   u -> [ f ]
  // ]

  private def groupUpdatableDefinitionsByFile(
    updatableDefinitions: Map[CandidateVersions, Iterable[DefinitionSite]]
  ): Map[File, Map[DefinitionSite, CandidateVersions]] =
    updatableDefinitions
      .map(_.swap)
      .flatMap(x => x._1.map(_ -> x._2))
      .groupBy(_._1.file)

}