package com.awwsmm.sbt.du.model

import com.timushev.sbt.updates.versions.Version

import scala.io.AnsiColor

sealed trait Bump

object Bump {

  // a Noop bump has no new version
  case object Noop extends Bump

  // a non-Noop bump has a new version, a level, and a pretty print method
  sealed trait NonEmpty extends Bump {
    def newVersion: Version

    def color: String

    def level: String

    def pretty = s"($color$level bump${AnsiColor.RESET})"
  }

  case class Patch(newVersion: Version) extends Bump.NonEmpty {
    // $COVERAGE-OFF$ -- not possible to target these specifically
    final override val level = "patch"
    final override val color = AnsiColor.GREEN
    // $COVERAGE-ON$
  }

  case class Minor(newVersion: Version) extends Bump.NonEmpty {
    // $COVERAGE-OFF$ -- not possible to target these specifically
    final override val level = "minor"
    final override val color = AnsiColor.YELLOW
    // $COVERAGE-ON$
  }

  case class Major(newVersion: Version) extends Bump.NonEmpty {
    // $COVERAGE-OFF$ -- not possible to target these specifically
    final override val level = "major"
    final override val color = AnsiColor.RED
    // $COVERAGE-ON$
  }

  // calculate the bump between an old and a new version
  def from(oldVersion: Version, newVersion: Version): Bump =
    if (oldVersion.major != newVersion.major) {
      Major(newVersion)

    } else if (oldVersion.minor != newVersion.minor) {
      Minor(newVersion)

    } else if (oldVersion.patch != newVersion.patch) {
      Patch(newVersion)

    } else {
      Noop
    }
}
