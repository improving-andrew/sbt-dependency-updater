package com.awwsmm.sbt.du.model

import sbt.File

// the definition site is the file and line index where the version string (e.g. "1.2.3") appears
case class DefinitionSite(file: File, lineIndex: Int)
