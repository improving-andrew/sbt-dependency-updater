package com.awwsmm.sbt.du.model

// mimics sbt.librarymanagement.DependencyBuilders.OrganizationArtifactName
case class Dependency(organization: String, artifact: String)
