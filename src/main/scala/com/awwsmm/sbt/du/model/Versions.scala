package com.awwsmm.sbt.du.model

import com.timushev.sbt.updates.versions.Version

import scala.collection.SortedSet
import scala.util.Try

// newer versions available for a dependency
case class Versions(current: Version, newer: SortedSet[Version]) {

  private val strings: SortedSet[String] = newer.map(_.text)

  private def maybeMax(versions: SortedSet[Version]): Option[Version] = Try(versions.max).toOption

  // the latest version with the same major and minor versions as the old version
  val maxPatchBump: Option[Version] =
    maybeMax {
      newer.filter { newVersion =>
        newVersion.major == current.major && newVersion.minor == current.minor
      }
    }

  // the latest version with the same major version as the old version, but a different minor version
  val maxMinorBump: Option[Version] =
    maybeMax {
      newer.filter { newVersion =>
        newVersion.major == current.major && newVersion.minor != current.minor
      }
    }

  // the latest version available overall
  val latest: Option[Version] = maybeMax(newer)

  // prints: (1.2.3 -> 1.2.4, 1.2.5, 1.3.0, 1.3.1, 1.4.0, 2.0.0)
  val updatePaths = s"""(${current.text} -> ${strings.mkString(", ")})"""

}
