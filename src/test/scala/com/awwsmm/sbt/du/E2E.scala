package com.awwsmm.sbt.du

import com.timushev.sbt.updates.versions.Version
import com.typesafe.config.ConfigFactory
import org.scalatest.Assertion
import org.scalatest.flatspec.AsyncFlatSpecLike
import org.scalatest.matchers.should
import sbt.librarymanagement.ModuleID

import java.io.{ByteArrayOutputStream, File, PrintStream}
import java.security.Permission
import scala.collection.SortedSet
import scala.util.{Failure, Success, Try}

class E2E extends AsyncFlatSpecLike with should.Matchers {

  behavior of "end-to-end behaviour"

  it should "update a single version string for a single dependency in a single file" in withContext() { (_, _, helper) =>

    // we can be pretty sure that this version string belongs to this dependency

    val dependencyUpdatesData = Map(
      ModuleID("org1", "art1", "0.0.0") -> SortedSet(Version("0.0.1"))
    )

    val file1 = new File("file1")

    val filesAndLines = Map(
      file1 ->
        """val v1 = "0.0.0"
          |""".stripMargin.split("\n").toSeq
    )

    val actual = helper.doEverything(dependencyUpdatesData, filesAndLines)

    val expected = Map(
      file1 ->
        """val v1 = "0.0.1"
          |""".stripMargin.split("\n").toSeq
    )

    actual shouldEqual expected
  }

  it should "update multiple unique version strings for multiple dependencies in a single file" in withContext() { (_, _, helper) =>

    // if "0.0.0" and "0.1.0" each both only appear once, we can be pretty sure they belong to these dependencies

    val dependencyUpdatesData = Map(
      ModuleID("org1", "art1", "0.0.0") -> SortedSet(Version("0.0.1")),
      ModuleID("org2", "art2", "0.1.0") -> SortedSet(Version("0.2.0"))
    )

    val file1 = new File("file1")

    val filesAndLines = Map(
      file1 ->
        """val v1 = "0.0.0"
          |val v2 = "0.1.0"
          |""".stripMargin.split("\n").toSeq
    )

    val actual = helper.doEverything(dependencyUpdatesData, filesAndLines)

    val expected = Map(
      file1 ->
        """val v1 = "0.0.1"
          |val v2 = "0.2.0"
          |""".stripMargin.split("\n").toSeq
    )

    actual shouldEqual expected
  }

  it should "fail to update duplicate version strings for a single dependency in a single file" in withContext() { (_, _, helper) =>

    // we can't know which of these "0.0.0" strings is associated with this dependency

    val dependencyUpdatesData = Map(
      ModuleID("org1", "art1", "0.0.0") -> SortedSet(Version("0.0.1"))
    )

    val file1 = new File("file1")

    val filesAndLines = Map(
      file1 ->
        """val v1 = "0.0.0"
          |val v2 = "0.0.0"
          |""".stripMargin.split("\n").toSeq
    )

    val actual = helper.doEverything(dependencyUpdatesData, filesAndLines)

    val expected = Map(
      file1 ->
        """val v1 = "0.0.0" // TODO 'sbt updateDependencies' cannot update org1:art1 because "0.0.0" is defined in multiple places; please update manually, and remove any unused version strings
          |val v2 = "0.0.0" // TODO 'sbt updateDependencies' cannot update org1:art1 because "0.0.0" is defined in multiple places; please update manually, and remove any unused version strings
          |""".stripMargin.split("\n").toSeq
    )

    actual shouldEqual expected
  }

  it should "update duplicate version strings for a single dependency across multiple files" in withContext() { (_, _, helper) =>

    // we assume this same version string is used for this same dependency, but for different modules in different files

    val dependencyUpdatesData = Map(
      ModuleID("org1", "art1", "0.0.0") -> SortedSet(Version("0.0.1"))
    )

    val file1 = new File("file1")
    val file2 = new File("file2")

    val filesAndLines = Map(
      file1 ->
        """val v1 = "0.0.0"
          |""".stripMargin.split("\n").toSeq,
      file2 ->
        """val v2 = "0.0.0"
          |""".stripMargin.split("\n").toSeq
    )

    val actual = helper.doEverything(dependencyUpdatesData, filesAndLines)

    val expected = Map(
      file1 ->
        """val v1 = "0.0.1"
          |""".stripMargin.split("\n").toSeq,
      file2 ->
        """val v2 = "0.0.1"
          |""".stripMargin.split("\n").toSeq
    )

    actual shouldEqual expected
  }

  it should "fail to update duplicate version strings for multiple dependencies in a single file" in withContext() { (_, _, helper) =>

    // we can't know which "0.0.0" belongs to which dependency... and they're upgrading to different versions

    val dependencyUpdatesData = Map(
      ModuleID("org1", "art1", "0.0.0") -> SortedSet(Version("0.0.1")),
      ModuleID("org2", "art2", "0.0.0") -> SortedSet(Version("0.2.0"))
    )

    val file1 = new File("file1")

    val filesAndLines = Map(
      file1 ->
        """val v1 = "0.0.0"
          |val v2 = "0.0.0"
          |""".stripMargin.split("\n").toSeq
    )

    val actual = helper.doEverything(dependencyUpdatesData, filesAndLines)

    val expected = Map(
      file1 ->
        """val v1 = "0.0.0" // TODO 'sbt updateDependencies' cannot update org1:art1, org2:art2 because "0.0.0" is ambiguous (it is being referenced by multiple dependencies), please update manually
          |val v2 = "0.0.0" // TODO 'sbt updateDependencies' cannot update org1:art1, org2:art2 because "0.0.0" is ambiguous (it is being referenced by multiple dependencies), please update manually
          |""".stripMargin.split("\n").toSeq
    )

    actual shouldEqual expected
  }

  it should "update multiple unique version strings for multiple dependencies across multiple files" in withContext() { (_, _, helper) =>

    // we should be able to update dependencies across multiple files

    val dependencyUpdatesData = Map(
      ModuleID("org1", "art1", "0.0.0") -> SortedSet(Version("0.0.1")),
      ModuleID("org2", "art2", "0.1.0") -> SortedSet(Version("0.2.0"))
    )

    val file1 = new File("file1")
    val file2 = new File("file2")

    val filesAndLines = Map(
      file1 ->
        """val v1 = "0.0.0"
          |""".stripMargin.split("\n").toSeq,
      file2 ->
        """val v2 = "0.1.0"
          |""".stripMargin.split("\n").toSeq
    )

    val actual = helper.doEverything(dependencyUpdatesData, filesAndLines)

    val expected = Map(
      file1 ->
        """val v1 = "0.0.1"
          |""".stripMargin.split("\n").toSeq,
      file2 ->
        """val v2 = "0.2.0"
          |""".stripMargin.split("\n").toSeq
    )

    actual shouldEqual expected
  }

  it should "fail to update duplicate version strings for multiple dependencies across multiple files" in withContext() { (_, _, helper) =>

    // we can't know which "0.0.0" belongs to which dependency... and they're upgrading to different versions

    val dependencyUpdatesData = Map(
      ModuleID("org1", "art1", "0.0.0") -> SortedSet(Version("0.0.1")),
      ModuleID("org2", "art2", "0.0.0") -> SortedSet(Version("0.2.0"))
    )

    val file1 = new File("file1")
    val file2 = new File("file2")

    val filesAndLines = Map(
      file1 ->
        """val v1 = "0.0.0"
          |""".stripMargin.split("\n").toSeq,
      file2 ->
        """val v2 = "0.0.0"
          |""".stripMargin.split("\n").toSeq
    )

    val actual = helper.doEverything(dependencyUpdatesData, filesAndLines)

    val expected = Map(
      file1 ->
        """val v1 = "0.0.0" // TODO 'sbt updateDependencies' cannot update org1:art1, org2:art2 because "0.0.0" is ambiguous (it is being referenced by multiple dependencies), please update manually
          |""".stripMargin.split("\n").toSeq,
      file2 ->
        """val v2 = "0.0.0" // TODO 'sbt updateDependencies' cannot update org1:art1, org2:art2 because "0.0.0" is ambiguous (it is being referenced by multiple dependencies), please update manually
          |""".stripMargin.split("\n").toSeq
    )

    actual shouldEqual expected
  }

  it should "update duplicate version strings for multiple dependencies in a single file if the target version is identical" in withContext() { (_, _, helper) =>

    // every "0.0.0" dependency is upgrading to "0.0.1", so it should be fine to change all "0.0.0" strings to "0.0.1"

    val dependencyUpdatesData = Map(
      ModuleID("org1", "art1", "0.0.0") -> SortedSet(Version("0.0.1")),
      ModuleID("org2", "art2", "0.0.0") -> SortedSet(Version("0.0.1"))
    )

    val file1 = new File("file1")

    val filesAndLines = Map(
      file1 ->
        """val v1 = "0.0.0"
          |val v2 = "0.0.0"
          |""".stripMargin.split("\n").toSeq
    )

    val actual = helper.doEverything(dependencyUpdatesData, filesAndLines)

    val expected = Map(
      file1 ->
        """val v1 = "0.0.1"
          |val v2 = "0.0.1"
          |""".stripMargin.split("\n").toSeq
    )

    actual shouldEqual expected
  }

  it should "update duplicate version strings for multiple dependencies across multiple files if the target version is identical" in withContext() { (_, _, helper) =>

    // every "0.0.0" dependency is upgrading to "0.0.1", so it should be fine to change all "0.0.0" strings to "0.0.1"

    val dependencyUpdatesData = Map(
      ModuleID("org1", "art1", "0.0.0") -> SortedSet(Version("0.0.1")),
      ModuleID("org2", "art2", "0.0.0") -> SortedSet(Version("0.0.1"))
    )

    val file1 = new File("file1")
    val file2 = new File("file2")

    val filesAndLines = Map(
      file1 ->
        """val v1 = "0.0.0"
          |""".stripMargin.split("\n").toSeq,
      file2 ->
        """val v2 = "0.0.0"
          |""".stripMargin.split("\n").toSeq
    )

    val actual = helper.doEverything(dependencyUpdatesData, filesAndLines)

    val expected = Map(
      file1 ->
        """val v1 = "0.0.1"
          |""".stripMargin.split("\n").toSeq,
      file2 ->
        """val v2 = "0.0.1"
          |""".stripMargin.split("\n").toSeq
    )

    actual shouldEqual expected
  }

  it should "fail to update a single version string for multiple dependencies in a single file" in withContext() { (_, _, helper) =>

    // there's only one "0.0.0" string, but there are two dependencies at "0.0.0", and they're updating to different versions!

    val dependencyUpdatesData = Map(
      ModuleID("org1", "art1", "0.0.0") -> SortedSet(Version("0.0.1")),
      ModuleID("org2", "art2", "0.0.0") -> SortedSet(Version("0.2.0"))
    )

    val file1 = new File("file1")

    val filesAndLines = Map(
      file1 ->
        """val v1 = "0.0.0"
          |""".stripMargin.split("\n").toSeq
    )

    val actual = helper.doEverything(dependencyUpdatesData, filesAndLines)

    val expected = Map(
      file1 ->
        """val v1 = "0.0.0" // TODO 'sbt updateDependencies' cannot update org1:art1, org2:art2 because "0.0.0" is ambiguous (it is being referenced by multiple dependencies), please update manually
          |""".stripMargin.split("\n").toSeq
    )

    actual shouldEqual expected
  }

  it should "update a single version string for multiple dependencies in a single file if the target version is identical" in withContext() { (_, _, helper) =>

    // there's only one "0.0.0" string, but there are two dependencies at "0.0.0", but they're updating to the same version

    val dependencyUpdatesData = Map(
      ModuleID("org1", "art1", "0.0.0") -> SortedSet(Version("0.0.1")),
      ModuleID("org2", "art2", "0.0.0") -> SortedSet(Version("0.0.1"))
    )

    val file1 = new File("file1")

    val filesAndLines = Map(
      file1 ->
        """val v1 = "0.0.0"
          |""".stripMargin.split("\n").toSeq
    )

    val actual = helper.doEverything(dependencyUpdatesData, filesAndLines)

    val expected = Map(
      file1 ->
        """val v1 = "0.0.1"
          |""".stripMargin.split("\n").toSeq
    )

    actual shouldEqual expected
  }

  behavior of "options"

  it should "follow the 'update' configuration provided" in withContext(
    config = s"""paths=[]\nupdate { never=["*1"]\npatchOnly=["*2"]\nminorOnly=["*3"]\nminorAndPatchOnly=["*4"] }"""
  ) { (_, _, helper) =>

    val dependencyUpdatesData = Map(
      ModuleID("org0", "art1", "0.0.0") -> SortedSet(Version("0.0.1"), Version("0.1.0"), Version( "1.0.0")), // never
      ModuleID("org1", "art2", "1.0.0") -> SortedSet(Version("1.0.1"), Version("1.1.0"), Version( "2.0.0")), // patchOnly
      ModuleID("org2", "art2", "2.0.0") -> SortedSet(                  Version("2.1.0"), Version( "3.0.0")), // patchOnly
      ModuleID("org3", "art3", "3.0.0") -> SortedSet(Version("3.0.1"), Version("3.1.0"), Version( "4.0.0")), // minorOnly
      ModuleID("org4", "art3", "4.0.0") -> SortedSet(Version("4.0.1"),                   Version( "5.0.0")), // minorOnly
      ModuleID("org5", "art4", "5.0.0") -> SortedSet(Version("5.0.1"), Version("5.1.0"), Version( "6.0.0")), // minorAndPatchOnly
      ModuleID("org6", "art4", "6.0.0") -> SortedSet(Version("6.0.1"),                   Version( "7.0.0")), // minorAndPatchOnly
      ModuleID("org7", "art4", "7.0.0") -> SortedSet(                  Version("7.1.0"), Version( "8.0.0")), // minorAndPatchOnly
      ModuleID("org8", "art4", "8.0.0") -> SortedSet(                                    Version( "9.0.0")), // minorAndPatchOnly
      ModuleID("org9", "art5", "9.0.0") -> SortedSet(Version("9.0.1"), Version("9.1.0"), Version("10.0.0"))  // no restrictions
    )

    val file1 = new File("file1")

    val filesAndLines = Map(
      file1 ->
        """val v1  = "0.0.0" // never
          |val v2  = "1.0.0" // patchOnly, with a patch bump available
          |val v3  = "2.0.0" // patchOnly, with no patch bump available
          |val v4  = "3.0.0" // minorOnly, with a minor bump available
          |val v5  = "4.0.0" // minorOnly, with no minor bump available
          |val v6  = "5.0.0" // minorAndPatchOnly, with minor and patch bumps available
          |val v7  = "6.0.0" // minorAndPatchOnly, with only a patch bump available
          |val v8  = "7.0.0" // minorAndPatchOnly, with only a minor bump available
          |val v9  = "8.0.0" // minorAndPatchOnly, with no minor or patch bumps available
          |val v10 = "9.0.0" // no restrictions
          |""".stripMargin.split("\n").toSeq
    )

    val actual = helper.doEverything(dependencyUpdatesData, filesAndLines)

    val expected = Map(
      file1 ->
        """val v1  = "0.0.0" // never
          |val v2  = "1.0.1" // patchOnly, with a patch bump available
          |val v3  = "2.0.0" // patchOnly, with no patch bump available
          |val v4  = "3.1.0" // minorOnly, with a minor bump available
          |val v5  = "4.0.0" // minorOnly, with no minor bump available
          |val v6  = "5.1.0" // minorAndPatchOnly, with minor and patch bumps available
          |val v7  = "6.0.1" // minorAndPatchOnly, with only a patch bump available
          |val v8  = "7.1.0" // minorAndPatchOnly, with only a minor bump available
          |val v9  = "8.0.0" // minorAndPatchOnly, with no minor or patch bumps available
          |val v10 = "10.0.0" // no restrictions
          |""".stripMargin.split("\n").toSeq
    )

    actual shouldEqual expected
  }

  it should "ignore non-SemVer bumps by default" in withContext() { (_, _, helper) =>

    val dependencyUpdatesData = Map(
      ModuleID("org0", "art1", "0.0.0") -> SortedSet(Version("0.1.0"), Version("1"))
    )

    val file1 = new File("file1")

    val filesAndLines = Map(
      file1 ->
        """val v1  = "0.0.0"
          |""".stripMargin.split("\n").toSeq
    )

    val actual = helper.doEverything(dependencyUpdatesData, filesAndLines)

    val expected = Map(
      file1 ->
        """val v1  = "0.1.0"
          |""".stripMargin.split("\n").toSeq
    )

    actual shouldEqual expected
  }

  it should "allow non-SemVer bumps when nonStrictSemVer = true" in withContext(config = "paths = []\noptions.nonStrictSemVer = true") { (_, _, helper) =>

    val dependencyUpdatesData = Map(
      ModuleID("org0", "art1", "0.0.0") -> SortedSet(Version("0.1.0"), Version("1"))
    )

    val file1 = new File("file1")

    val filesAndLines = Map(
      file1 ->
        """val v1  = "0.0.0"
          |""".stripMargin.split("\n").toSeq
    )

    val actual = helper.doEverything(dependencyUpdatesData, filesAndLines)

    val expected = Map(
      file1 ->
        """val v1  = "1"
          |""".stripMargin.split("\n").toSeq
    )

    actual shouldEqual expected
  }

  it should "System.exit when options.check = true and there are out-of-date dependencies" in withContext(config = "paths = []\noptions.check = true") { (_, _, helper) =>

    val dependencyUpdatesData = Map(
      ModuleID("org0", "art1", "0.0.0") -> SortedSet(Version("0.1.0"), Version("1"))
    )

    val file1 = new File("file1")

    val filesAndLines = Map(
      file1 ->
        """val v1  = "0.0.0"
          |""".stripMargin.split("\n").toSeq
    )

    val systemExit = new RuntimeException("in lieu of System.exit")

    // adapted from https://www.baeldung.com/junit-system-exit
    class NoExitSecurityManager extends SecurityManager {
      override def checkPermission(perm: Permission): Unit = ()

      override def checkExit(status: Int): Unit = {
        super.checkExit(status)
        throw systemExit
      }
    }

    val originalSecurityManager = System.getSecurityManager

    System.setSecurityManager(new NoExitSecurityManager())

    val returned = Try {
      helper.doEverything(dependencyUpdatesData, filesAndLines)
    }

    System.setSecurityManager(originalSecurityManager)

    returned match {
      case Failure(exception) => exception shouldEqual systemExit
      case Success(_) => fail()
    }
  }

  def withContext(args: Seq[String] = Seq(), config: String = "paths = []")(f: (ByteArrayOutputStream, ByteArrayOutputStream, Helper) => Assertion): Assertion = {
    val out = new ByteArrayOutputStream()
    val err = new ByteArrayOutputStream()
    val context = new Context(args, ConfigFactory.parseString(config), new PrintStream(out), new PrintStream(err))
    val helper = new Helper(context)
    f(out, err, helper)
  }

}
