package com.awwsmm.sbt.du.model

import com.timushev.sbt.updates.versions.Version
import org.scalatest.flatspec.AsyncFlatSpecLike
import org.scalatest.matchers.should

import scala.io.AnsiColor

class BumpSpec extends AsyncFlatSpecLike with should.Matchers {

  behavior of "Bump"

  it should "correctly determine major version bumps" in {
    val oldVersion = Version("1.2.3")
    val newVersion = Version("2.3.4")

    val actual = Bump.from(oldVersion, newVersion)
    val expected = Bump.Major(newVersion)

    actual shouldEqual expected
  }

  it should "correctly determine minor version bumps" in {
    val oldVersion = Version("1.2.3")
    val newVersion = Version("1.3.4")

    val actual = Bump.from(oldVersion, newVersion)
    val expected = Bump.Minor(newVersion)

    actual shouldEqual expected
  }

  it should "correctly determine patch version bumps" in {
    val oldVersion = Version("1.2.3")
    val newVersion = Version("1.2.4")

    val actual = Bump.from(oldVersion, newVersion)
    val expected = Bump.Patch(newVersion)

    actual shouldEqual expected
  }

  it should "correctly determine noop version bumps" in {
    val oldVersion = Version("1.2.3")
    val newVersion = Version("1.2.3")

    val actual = Bump.from(oldVersion, newVersion)
    val expected = Bump.Noop

    actual shouldEqual expected
  }

  it should "pretty-print a major version bump" in {
    val bump: Bump.Major = Bump.Major(Version("1.2.3"))

    bump.level shouldEqual "major"
    bump.color shouldEqual AnsiColor.RED
    bump.pretty shouldEqual s"(${AnsiColor.RED}major bump${AnsiColor.RESET})"
  }

  it should "set a minor version bump's level and colour" in {
    val bump: Bump.Minor = Bump.Minor(Version("1.2.3"))

    bump.level shouldEqual "minor"
    bump.color shouldEqual AnsiColor.YELLOW
    bump.pretty shouldEqual s"(${AnsiColor.YELLOW}minor bump${AnsiColor.RESET})"
  }

  it should "set a patch version bump's level and colour" in {
    val bump: Bump.Patch = Bump.Patch(Version("1.2.3"))

    bump.level shouldEqual "patch"
    bump.color shouldEqual AnsiColor.GREEN
    bump.pretty shouldEqual s"(${AnsiColor.GREEN}patch bump${AnsiColor.RESET})"
  }

}
