package com.awwsmm.sbt.du.model

import com.awwsmm.sbt.du.Context
import com.timushev.sbt.updates.versions.Version
import com.typesafe.config.ConfigFactory
import org.scalatest.flatspec.AsyncFlatSpecLike
import org.scalatest.matchers.should

import java.io.{ByteArrayOutputStream, PrintStream}
import scala.collection.SortedSet
import scala.io.AnsiColor

class ContextSpec extends AsyncFlatSpecLike with should.Matchers {

  behavior of "Context"

  behavior of "options"

  it should "disable all options by default" in {
    val args = Seq.empty
    val config = ConfigFactory.parseString("paths = []")
    val context = new Context(args, config)

    context.options.check shouldBe false
    context.options.debug shouldBe false
    context.options.dryRun shouldBe false
    context.options.nonStrictSemVer shouldBe false
    context.options.silent shouldBe false
  }

  it should "enable 'check' if it's sent as a command-line argument" in {
    val args = Seq("check")
    val config = ConfigFactory.parseString("paths = []")
    val context = new Context(args, config)
    context.options.check shouldBe true
  }

  it should "enable 'debug' if it's sent as a command-line argument" in {
    val args = Seq("debug")
    val config = ConfigFactory.parseString("paths = []")
    val context = new Context(args, config)
    context.options.debug shouldBe true
  }

  it should "enable 'dryRun' if it's sent as a command-line argument" in {
    val args = Seq("dryRun")
    val config = ConfigFactory.parseString("paths = []")
    val context = new Context(args, config)
    context.options.dryRun shouldBe true
  }

  it should "enable 'nonStrictSemVer' if it's sent as a command-line argument" in {
    val args = Seq("nonStrictSemVer")
    val config = ConfigFactory.parseString("paths = []")
    val context = new Context(args, config)
    context.options.nonStrictSemVer shouldBe true
  }

  it should "enable 'silent' if it's sent as a command-line argument" in {
    val args = Seq("silent")
    val config = ConfigFactory.parseString("paths = []")
    val context = new Context(args, config)
    context.options.silent shouldBe true
  }

  it should "enable multiple command-line options at once" in {
    val args = Seq("check", "debug", "dryRun", "nonStrictSemVer", "silent")
    val config = ConfigFactory.parseString("paths = []")
    val context = new Context(args, config)

    context.options.check shouldBe true
    context.options.debug shouldBe true
    context.options.dryRun shouldBe true
    context.options.nonStrictSemVer shouldBe true
    context.options.silent shouldBe true
  }

  it should "enable 'check' if it's set to 'true' in the configuration file" in {
    val args = Seq.empty
    val config = ConfigFactory.parseString("options.check = true\npaths = []")
    val context = new Context(args, config)
    context.options.check shouldBe true
  }

  it should "enable 'debug' if it's set to 'true' in the configuration file" in {
    val args = Seq.empty
    val config = ConfigFactory.parseString("options.debug = true\npaths = []")
    val context = new Context(args, config)
    context.options.debug shouldBe true
  }

  it should "enable 'dryRun' if it's set to 'true' in the configuration file" in {
    val args = Seq.empty
    val config = ConfigFactory.parseString("options.dryRun = true\npaths = []")
    val context = new Context(args, config)
    context.options.dryRun shouldBe true
  }

  it should "enable 'nonStrictSemVer' if it's set to 'true' in the configuration file" in {
    val args = Seq.empty
    val config = ConfigFactory.parseString("options.nonStrictSemVer = true\npaths = []")
    val context = new Context(args, config)
    context.options.nonStrictSemVer shouldBe true
  }

  it should "enable 'silent' if it's set to 'true' in the configuration file" in {
    val args = Seq.empty
    val config = ConfigFactory.parseString("options.silent = true\npaths = []")
    val context = new Context(args, config)
    context.options.silent shouldBe true
  }

  it should "enable multiple configuration file options at once" in {
    val args = Seq.empty

    val configFile =
      """
        |options {
        |  check = true
        |  debug = true
        |  dryRun = true
        |  nonStrictSemVer = true
        |  silent = true
        |}
        |
        |paths = []
        |""".stripMargin

    val config = ConfigFactory.parseString(configFile)
    val context = new Context(args, config)

    context.options.check shouldBe true
    context.options.debug shouldBe true
    context.options.dryRun shouldBe true
    context.options.nonStrictSemVer shouldBe true
    context.options.silent shouldBe true
  }

  behavior of "toPattern"

  it should "convert globs into proper regex" in {
    val actual = Context.toPattern("hello:*").toString
    val expected = "hello:.*"

    actual shouldEqual expected
  }

  behavior of "createFilterFor"

  it should "correctly convert a simple Pattern into an OrganizationArtifactName predicate" in {
    val pattern = Context.toPattern("hello:goodbye")
    val matcher = Context.createFilterFor(Seq(pattern))

    matcher(Dependency("hello", "goodbye")) shouldBe true
    matcher(Dependency("hello", "hello")) shouldBe false
  }

  it should "correctly convert a Pattern with a glob into an OrganizationArtifactName predicate" in {
    val pattern = Context.toPattern("hello:*")
    val matcher = Context.createFilterFor(Seq(pattern))

    matcher(Dependency("hello", "goodbye")) shouldBe true
    matcher(Dependency("hello", "hello")) shouldBe true
  }

  behavior of "update"

  it should "never allow updates for dependencies configured as such" in {
    val args = Seq.empty

    val configFile =
      """
        |update {
        |  never = [
        |    "hello:*"
        |  ]
        |}
        |
        |paths = []
        |""".stripMargin

    val config = ConfigFactory.parseString(configFile)
    val context = new Context(args, config)

    context.update.never(Dependency("hello", "goodbye")) shouldBe true
    context.update.never(Dependency("goodbye", "goodbye")) shouldBe false
  }

  it should "only allow patch updates for dependencies configured as such" in {
    val args = Seq.empty

    val configFile =
      """
        |update {
        |  patchOnly = [
        |    "hello:*"
        |  ]
        |}
        |
        |paths = []
        |""".stripMargin

    val config = ConfigFactory.parseString(configFile)
    val context = new Context(args, config)

    context.update.patchOnly(Dependency("hello", "goodbye")) shouldBe true
    context.update.patchOnly(Dependency("goodbye", "goodbye")) shouldBe false
  }

  it should "only allow minor updates for dependencies configured as such" in {
    val args = Seq.empty

    val configFile =
      """
        |update {
        |  minorOnly = [
        |    "hello:*"
        |  ]
        |}
        |
        |paths = []
        |""".stripMargin

    val config = ConfigFactory.parseString(configFile)
    val context = new Context(args, config)

    context.update.minorOnly(Dependency("hello", "goodbye")) shouldBe true
    context.update.minorOnly(Dependency("goodbye", "goodbye")) shouldBe false
  }

  it should "only allow minor and patch updates for dependencies configured as such" in {
    val args = Seq.empty

    val configFile =
      """
        |update {
        |  minorAndPatchOnly = [
        |    "hello:*"
        |  ]
        |}
        |
        |paths = []
        |""".stripMargin

    val config = ConfigFactory.parseString(configFile)
    val context = new Context(args, config)

    context.update.minorAndPatchOnly(Dependency("hello", "goodbye")) shouldBe true
    context.update.minorAndPatchOnly(Dependency("goodbye", "goodbye")) shouldBe false
  }

  it should "if a dependency matches 'never' and also 'patchOnly', update never" in {
    val args = Seq.empty

    val configFile =
      """
        |update {
        |  never = [
        |    "hello:goodbye"
        |  ]
        |  patchOnly = [
        |    "hello:*"
        |  ]
        |}
        |
        |paths = []
        |""".stripMargin

    val config = ConfigFactory.parseString(configFile)
    val context = new Context(args, config)

    context.update.never(Dependency("hello", "goodbye")) shouldBe true
    context.update.patchOnly(Dependency("hello", "hello")) shouldBe true
  }

  it should "if a dependency matches 'never' and also 'minorOnly', update never" in {
    val args = Seq.empty

    val configFile =
      """
        |update {
        |  never = [
        |    "hello:goodbye"
        |  ]
        |  minorOnly = [
        |    "hello:*"
        |  ]
        |}
        |
        |paths = []
        |""".stripMargin

    val config = ConfigFactory.parseString(configFile)
    val context = new Context(args, config)

    context.update.never(Dependency("hello", "goodbye")) shouldBe true
    context.update.minorOnly(Dependency("hello", "hello")) shouldBe true
  }

  it should "if a dependency matches 'never' and also 'minorAndPatchOnly', update never" in {
    val args = Seq.empty

    val configFile =
      """
        |update {
        |  never = [
        |    "hello:goodbye"
        |  ]
        |  minorAndPatchOnly = [
        |    "hello:*"
        |  ]
        |}
        |
        |paths = []
        |""".stripMargin

    val config = ConfigFactory.parseString(configFile)
    val context = new Context(args, config)

    context.update.never(Dependency("hello", "goodbye")) shouldBe true
    context.update.minorAndPatchOnly(Dependency("hello", "hello")) shouldBe true
  }

  it should "if a dependency matches 'patchOnly' and also 'minorOnly', update on patch bumps only" in {
    val args = Seq.empty

    val configFile =
      """
        |update {
        |  patchOnly = [
        |    "hello:goodbye"
        |  ]
        |  minorOnly = [
        |    "hello:*"
        |  ]
        |}
        |
        |paths = []
        |""".stripMargin

    val config = ConfigFactory.parseString(configFile)
    val context = new Context(args, config)

    context.update.patchOnly(Dependency("hello", "goodbye")) shouldBe true
    context.update.minorOnly(Dependency("hello", "hello")) shouldBe true
  }

  it should "if a dependency matches 'patchOnly' and also 'minorAndPatchOnly', update on patch bumps only" in {
    val args = Seq.empty

    val configFile =
      """
        |update {
        |  patchOnly = [
        |    "hello:goodbye"
        |  ]
        |  minorAndPatchOnly = [
        |    "hello:*"
        |  ]
        |}
        |
        |paths = []
        |""".stripMargin

    val config = ConfigFactory.parseString(configFile)
    val context = new Context(args, config)

    context.update.patchOnly(Dependency("hello", "goodbye")) shouldBe true
    context.update.minorAndPatchOnly(Dependency("hello", "hello")) shouldBe true
  }

  it should "if a dependency matches 'minorOnly' and also 'minorAndPatchOnly', update on minor bumps only" in {
    val args = Seq.empty

    val configFile =
      """
        |update {
        |  minorOnly = [
        |    "hello:goodbye"
        |  ]
        |  minorAndPatchOnly = [
        |    "hello:*"
        |  ]
        |}
        |
        |paths = []
        |""".stripMargin

    val config = ConfigFactory.parseString(configFile)
    val context = new Context(args, config)

    context.update.minorOnly(Dependency("hello", "goodbye")) shouldBe true
    context.update.minorAndPatchOnly(Dependency("hello", "hello")) shouldBe true
  }

  behavior of "isStrictSemVer"

  it should "return true if the provided String represents a strict semantic version" in {
    Context.isStrictSemVer("1.2.3") shouldBe true
  }

  it should "return false if the provided String does not represent a strict semantic version" in {
    Context.isStrictSemVer("1.2.3.4") shouldBe false
    Context.isStrictSemVer("1.2.3-SNAPSHOT") shouldBe false
  }

  behavior of "debug"

  it should "not print debug logs to stdout when 'debug' is not enabled" in {
    val args = Seq.empty
    val config = ConfigFactory.parseString("paths = []")

    val stdout = new ByteArrayOutputStream()
    val context = new Context(args, config, new PrintStream(stdout))

    context.debug("test")
    val expected = ""
    stdout.toString() shouldEqual expected
  }

  it should "print debug logs to stdout when 'debug' is enabled" in {
    val args = Seq("debug")
    val config = ConfigFactory.parseString("paths = []")

    val stdout = new ByteArrayOutputStream()
    val context = new Context(args, config, new PrintStream(stdout))

    context.debug("test")
    val expected = s"[${AnsiColor.MAGENTA}updateDependencies debug${AnsiColor.RESET}] test\n"
    stdout.toString() shouldEqual expected
  }

  behavior of "log"

  it should "not print logs to stdout when 'silent' is enabled" in {
    val args = Seq("silent")
    val config = ConfigFactory.parseString("paths = []")

    val stdout = new ByteArrayOutputStream()
    val context = new Context(args, config, new PrintStream(stdout))

    context.log("test")
    val expected = ""
    stdout.toString() shouldEqual expected
  }

  it should "print logs to stdout when 'silent' is not enabled" in {
    val args = Seq.empty
    val config = ConfigFactory.parseString("paths = []")

    val stdout = new ByteArrayOutputStream()
    val context = new Context(args, config, new PrintStream(stdout))

    context.log("test")
    val expected = s"[${AnsiColor.CYAN}updateDependencies${AnsiColor.RESET}] test\n"
    stdout.toString() shouldEqual expected
  }

  behavior of "error"

  it should "print logs to stderr, even when 'silent' is enabled" in {
    val args = Seq("silent")
    val config = ConfigFactory.parseString("paths = []")

    val stderr = new ByteArrayOutputStream()
    val context = new Context(args, config, err = new PrintStream(stderr))

    context.error("test")
    val expected = "test\n"
    stderr.toString() shouldEqual expected
  }

  behavior of "whisper"

  it should "not print logs to stdout when 'silent' is enabled" in {
    val args = Seq("silent")
    val config = ConfigFactory.parseString("paths = []")

    val stdout = new ByteArrayOutputStream()
    val context = new Context(args, config, new PrintStream(stdout))

    context.whisper("test")
    val expected = ""
    stdout.toString() shouldEqual expected
  }

  it should "print logs to stdout when 'silent' is not enabled" in {
    val args = Seq.empty
    val config = ConfigFactory.parseString("paths = []")

    val stdout = new ByteArrayOutputStream()
    val context = new Context(args, config, new PrintStream(stdout))

    context.whisper("test")
    val expected = s"[${AnsiColor.CYAN}updateDependencies${AnsiColor.RESET}] ${AnsiColor.BLACK}test${AnsiColor.RESET}\n"
    stdout.toString() shouldEqual expected
  }

  behavior of "willNotUpdate"

  it should "pretty-print a string explaining why a dependency won't be updated" in {
    val args = Seq.empty
    val config = ConfigFactory.parseString("paths = []")

    val dependency = Dependency("hello", "goodbye")
    val updatesExist = "so, so many"

    val versions = SortedSet(
      Version("1.2.4"),
      Version("1.2.5"),
      Version("1.3.0"),
      Version("1.4.0"),
      Version("2.0.0"),
      Version("3.0.0")
    )

    val newerVersions = Versions(Version("1.2.3"), versions)

    val stdout = new ByteArrayOutputStream()
    val context = new Context(args, config, new PrintStream(stdout))

    context.willNotUpdate(dependency, updatesExist, newerVersions)
    val text = "will not update goodbye per configuration, though so, so many updates exist (1.2.3 -> 1.2.4, 1.2.5, 1.3.0, 1.4.0, 2.0.0, 3.0.0)"
    val expected = s"[${AnsiColor.CYAN}updateDependencies${AnsiColor.RESET}] ${AnsiColor.BLACK}$text${AnsiColor.RESET}\n"
    stdout.toString() shouldEqual expected
  }

}
