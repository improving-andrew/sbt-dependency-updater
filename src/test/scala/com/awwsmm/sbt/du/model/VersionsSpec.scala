package com.awwsmm.sbt.du.model

import com.timushev.sbt.updates.versions.Version
import org.scalatest.flatspec.AsyncFlatSpecLike
import org.scalatest.matchers.should

import scala.collection.SortedSet

class VersionsSpec extends AsyncFlatSpecLike with should.Matchers {

  behavior of "VersionAndDescendants"

  private val versions = SortedSet(
    Version("1.2.3"),
    Version("1.2.4"),
    Version("1.3.3"),
    Version("1.4.3"),
    Version("2.2.3")
  )

  val newerVersions: Versions = Versions(Version("1.2.2"), versions)

  it should "correctly report the maximum patch bump" in {
    val actual = newerVersions.maxPatchBump
    val expected = Version("1.2.4")

    actual shouldEqual Some(expected)
  }

  it should "correctly report the maximum minor bump" in {
    val actual = newerVersions.maxMinorBump
    val expected = Version("1.4.3")

    actual shouldEqual Some(expected)
  }

  it should "correctly report the latest version" in {
    val actual = newerVersions.latest
    val expected = Version("2.2.3")

    actual shouldEqual Some(expected)
  }

  it should "nicely format possible upgrade paths" in {
    val actual = newerVersions.updatePaths
    val expected = "(1.2.2 -> 1.2.3, 1.2.4, 1.3.3, 1.4.3, 2.2.3)"

    actual shouldEqual expected
  }

}
